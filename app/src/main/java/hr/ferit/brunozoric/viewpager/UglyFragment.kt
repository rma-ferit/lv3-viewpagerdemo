package hr.ferit.brunozoric.viewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class UglyFragment : Fragment() {

    companion object {
        fun newInstance(): UglyFragment{
            return UglyFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_ugly, container, false);
        return view;
    }
}
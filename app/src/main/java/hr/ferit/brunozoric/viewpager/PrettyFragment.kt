package hr.ferit.brunozoric.viewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class PrettyFragment : Fragment() {

    companion object {
        fun newInstance(): PrettyFragment{
            return PrettyFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pretty, container, false);
        return view;
    }
}